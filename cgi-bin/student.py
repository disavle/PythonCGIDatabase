#!/usr/bin/env python3
import cgi
import pymysql
import html
from config import host, user, password, db_name

form = cgi.FieldStorage()

nameInput = form.getfirst("nameInput", "")
dateInput = form.getfirst("dateInput", "")
facultyInput = form.getfirst("facultyInput", "")
specialityInput = form.getfirst("specialityInput", "")
courseInput = form.getfirst("courseInput", "")
groupNamInput = form.getfirst("groupNamInput", "")
action = form.getfirst("action", "")
save = form.getfirst("save", "")
delete = form.getfirst("delete", "")

nameInput = html.escape(nameInput)
dateInput = html.escape(dateInput)
facultyInput = html.escape(facultyInput)
specialityInput = html.escape(specialityInput)
courseInput = html.escape(courseInput)
groupNamInput = html.escape(groupNamInput)

def getAll():
    try:
        connection = pymysql.connect(
            host=host,
            port=3306,
            user=user,
            password=password,
            database=db_name,
            cursorclass=pymysql.cursors.DictCursor
        )
        try:
            with connection.cursor() as cursor:
                create_table_qyery = """CREATE TABLE IF NOT EXISTS faculty (id int primary key AUTO_INCREMENT NOT NULL,
                name varchar(255) NOT NULL);"""
                cursor.execute(create_table_qyery)
            with connection.cursor() as cursor:
                create_table_qyery = """CREATE TABLE IF NOT EXISTS speciality (id int primary key AUTO_INCREMENT NOT NULL,
                name varchar(255) NOT NULL);"""
                cursor.execute(create_table_qyery)
            with connection.cursor() as cursor:
                create_table_qyery = """CREATE TABLE IF NOT EXISTS student (id int primary key AUTO_INCREMENT NOT NULL,
                name varchar(255) NOT NULL,
                dateIn date not null,
                facultyId int,
                specialityId int,
                course int NOT NULL,
                groupNum varchar(255) NOT NULL,
                FOREIGN KEY (facultyId) REFERENCES faculty(id),
                FOREIGN KEY (specialityId) REFERENCES speciality(id)
                );"""
                cursor.execute(create_table_qyery)
            if action == "add":
                with connection.cursor() as cursor:
                    create_table_qyery = "insert into student (name, dateIn, facultyId, specialityId, course, groupNum) values('"+nameInput+"','"+dateInput+"','"+facultyInput+"','"+specialityInput+"','"+courseInput+"','"+groupNamInput+"');"
                    cursor.execute(create_table_qyery)
                    connection.commit()
            if delete != "":
                with connection.cursor() as cursor:
                    create_table_qyery = "delete from student where id = "+delete+";"
                    cursor.execute(create_table_qyery)
                    connection.commit()
            if save != "":
                with connection.cursor() as cursor:
                    create_table_qyery = "update student set name = '" + nameInput + "', dateIn = '" + dateInput + "', facultyId = '" + facultyInput + "', specialityId = '" + specialityInput + "', course = '" + courseInput + "', groupNum = '" + groupNamInput + "' where id = "+save+";"
                    cursor.execute(create_table_qyery)
                    connection.commit()
            with connection.cursor() as cursor:
                create_table_qyery = "SELECT * FROM student order by id"
                cursor.execute(create_table_qyery)
                rows = cursor.fetchall()
                for row in rows:
                    print(
                        """
                        <tr>
                            <th scope="row">""" + str(row["id"]) + """</th>
                            <td>""" + str(row["name"]) + """</td>
                            <td>""" + str(row["dateIn"]) + """</td>
                            <td>""" + str(row["facultyId"]) + """</td>
                            <td>""" + str(row["specialityId"]) + """</td>
                            <td>""" + str(row["course"]) + """</td>
                            <td>""" + str(row["groupNum"]) + """</td>
                            <td>
                                <div class="col">
                                    <form action="/cgi-bin/student.py">
                                    <button type="submit" class="btn btn-light" data-bs-toggle="modal"
                                    data-bs-target="#editModal">Изменить</button>
                                    <input type="hidden" name="action" value="""+str(row["id"])+""">
                                    </form>
                                </div>
                            </td>
                        </tr>
                        """
                    )
        finally:
            connection.close()
    except Exception as ex:
        print("Connection refused...")
        print(ex)

def editRow():
    try:
        connection = pymysql.connect(
            host=host,
            port=3306,
            user=user,
            password=password,
            database=db_name,
            cursorclass=pymysql.cursors.DictCursor
        )
        try:
            if action == "edit":
                pass
            if action != "add"  and action != "":
                with connection.cursor() as cursor:
                    create_table_qyery = "SELECT * FROM student where id = '"+action+"'"
                    cursor.execute(create_table_qyery)
                    rows = cursor.fetchall()
                    for row in rows:
                        print("""
                          <div class="modal fade" id="editModal" tabindex="-1" aria-labelledby="editModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                
               <form class="container-fluid" action="/cgi-bin/student.py">
               <div class="modal-header">
                    <h5 class="modal-title" id="editModalLabel">Изменение строки</h5>
                    
                    <button type="submit" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                         <div class="input-group mb-3">
                            <span class="input-group-text" id="basic-addon1">Name</span>
                            <input type="text" name="nameInput" class="form-control" placeholder="Name" aria-label="Name" aria-describedby="basic-addon1" required value='"""+str(row["name"])+"""'>
                        </div>
                        <div class="input-group mb-3">
                            <span class="input-group-text" id="basic-addon1">DateIn</span>
                            <input type="text" name="dateInput" class="form-control" pattern="\d{4}-\d{1,2}-\d{1,2}" placeholder="DateIn" aria-label="DateIn" aria-describedby="basic-addon1" required value='"""+str(row["dateIn"])+"""'>
                        </div>
                        <div class="input-group mb-3">
                            <span class="input-group-text" id="basic-addon1">FacultyId</span>
                            <input type="text" pattern="[0-9]+" name="facultyInput" class="form-control" placeholder="FacultyId" aria-label="FacultyId" aria-describedby="basic-addon1" required value='"""+str(row["facultyId"])+"""'>
                        </div>
                        <div class="input-group mb-3">
                            <span class="input-group-text" id="basic-addon1">SpecialityId</span>
                            <input type="text" pattern="[0-9]+" name="specialityInput" class="form-control" placeholder="SpecialityId" aria-label="SpecialityId" aria-describedby="basic-addon1" required value='"""+str(row["specialityId"])+"""'>
                        </div>
                        <div class="input-group mb-3">
                            <span class="input-group-text" id="basic-addon1">Course</span>
                            <input type="text" pattern="[0-9]+" name="courseInput" class="form-control" placeholder="Course" aria-label="Course" aria-describedby="basic-addon1" required value='"""+str(row["course"])+"""'>
                        </div>
                        <div class="input-group mb-3">
                            <span class="input-group-text" id="basic-addon1">GroupNum</span>
                            <input type="text" name="groupNamInput" class="form-control" placeholder="GroupNum" aria-label="GroupNum" aria-describedby="basic-addon1" required value='"""+str(row["groupNum"])+"""'>
                        </div>
                         </div>
                <div class="modal-footer">
                    <button type="submit" name="delete" value='"""+action+"""' class="btn-sm btn-danger" data-bs-dismiss="modal">Удалить</button>
                    <button type="submit" name="save" value='"""+action+"""' class="btn btn-dark">Сохранить</button>
                </div>
                <script type="text/javascript">
    window.onload = function () {
        OpenBootstrapPopup();
    };
    function OpenBootstrapPopup() {
        $("#editModal").modal('show');
    }
</script>
                </form>
            </div>
        </div>
    </div>
                        """)
        finally:
            connection.close()
    except Exception as ex:
        print("Connection refused...")
        print(ex)

print("""
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
        crossorigin="anonymous"></script>
         <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <title>Python</title>
</head>

<body class="container-fluid p-0 w-100">
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark sticky-top">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">Database</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle active" href="#" id="navbarDropdownMenuLink" role="button"
                            data-bs-toggle="dropdown" aria-expanded="false">
                            Таблицы
                        </a>
                        <ul class="dropdown-menu " aria-labelledby="navbarDropdownMenuLink">
                            <li><a class="dropdown-item active" type="submit" href="/cgi-bin/student.py">Студент</a></li>
                            <li><a class="dropdown-item" href="/cgi-bin/faculty.py">Факультет</a></li>
                            <li><a class="dropdown-item" href="/cgi-bin/speciality.py">Специальность</a></li>
                        </form>
                            
                            
                        </ul>
                    </li>
                    <li class="nav-item">
                        <div class="col">
                            <form action="/cgi-bin/student.py">
                                <button type="submit"  class="btn btn-light" >Обновить</button>
                                <button type="button" class="btn btn-light" data-bs-toggle="modal"
                            data-bs-target="#addModal" data-bs-whatever="@mdo">Добавить</button>
                            </form>
                        </div>
                        
                    </li>
                 
                </ul>
            </div>
        </div>
    </nav>
    <div class="d-flex justify-content-center mt-1">
        <table class="table table-dark table-hover mb-0">
            <thead>
                <tr>
                    <th scope="col">StudentId</th>
                    <th scope="col">Name</th>
                    <th scope="col">DateIn</th>
                    <th scope="col">FacultyId</th>
                    <th scope="col">SpecialityId</th>
                    <th scope="col">Course</th>
                    <th scope="col">Group</th>
                    <th scope="col">Действие</th>
                </tr>
            </thead>
            <tbody>
""")
getAll()
print("""
            </tbody>
        </table>
    </div>

    <div class="modal fade" id="addModal" tabindex="-1" aria-labelledby="addModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="addModalLabel">Добавление строки</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                 <form class="container-fluid" action="/cgi-bin/student.py">
                <div class="modal-body">

                        <div class="input-group mb-3">
                            <span class="input-group-text" id="basic-addon1">Name</span>
                            <input type="text" name="nameInput" class="form-control" placeholder="Name" aria-label="Name" aria-describedby="basic-addon1" required>
                        </div>
                        <div class="input-group mb-3">
                            <span class="input-group-text" id="basic-addon1">DateIn</span>
                            <input type="text" name="dateInput" class="form-control" pattern="\d{4}-\d{1,2}-\d{1,2}" placeholder="DateIn" aria-label="DateIn" aria-describedby="basic-addon1" required>
                        </div>
                        <div class="input-group mb-3">
                            <span class="input-group-text" id="basic-addon1">FacultyId</span>
                            <input type="text" pattern="[0-9]+" name="facultyInput" class="form-control" placeholder="FacultyId" aria-label="FacultyId" aria-describedby="basic-addon1" required>
                        </div>
                        <div class="input-group mb-3">
                            <span class="input-group-text" id="basic-addon1">SpecialityId</span>
                            <input type="text" pattern="[0-9]+" name="specialityInput" class="form-control" placeholder="SpecialityId" aria-label="SpecialityId" aria-describedby="basic-addon1" required>
                        </div>
                        <div class="input-group mb-3">
                            <span class="input-group-text" id="basic-addon1">Course</span>
                            <input type="text" pattern="[0-9]+" name="courseInput" class="form-control" placeholder="Course" aria-label="Course" aria-describedby="basic-addon1" required>
                        </div>
                        <div class="input-group mb-3">
                            <span class="input-group-text" id="basic-addon1">GroupNum</span>
                            <input type="text" name="groupNamInput" class="form-control" placeholder="GroupNum" aria-label="GroupNum" aria-describedby="basic-addon1" required>
                        </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn-sm btn-danger" data-bs-dismiss="modal">Отмена</button>
                    <input type="hidden" name="action" value="add">
                    <button type="submit" class="btn btn-dark">Добавить</button>
                </div>
                </form>
            </div>
        </div>
    </div>
  
""")
editRow()
print("""
</body>

</html>
""")

